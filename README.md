# Equo-Application-Menu

Equo-Application-Menu is a node package with the name '@equo/equo-application-menu' that allows through an api a native constructor of menus for Equo applications, in a chained way.

see [more](how-to-include-equo-components.md) about how to include Equo component in your projects.

### Menu
- [`create() MenuBuilder`](#create)
- [getCurrentModel() void](#getCurrentModel)

- ### create
	- Returns: 
		- MenuBuilder
	- Description:
		- Create menu instance.
	- [Use example](equo-application-menu-examples.md#Using-the-create-method.)
	<br>

- ### getCurrentModel
	- Params:
		- callback :(menuBuilder MenuBuilder) {}
	- Returns:
		- void
	- Description: 
		- Insert new root menu in current menu model. You can build a new menu model from the one established in your application using a callback as a parameter.
	- [Use example](equo-application-menu-examples.md#Using-the-getCurrentModel-method.)
	<br>

### MenuBuilder
- [getMenus() Array\<EquoMenu>](-#getMenus)
- [withMainMenu() MenuBuilder](#withMainMenu)
- [addMenu() MenuBuilder](#addMenu)
- [apendMenu() MenuBuilder | null](#apendMenu)
- [addMenuItem() MenuItemBuilder](#addMenuItem)
- [apendMenuItem() MenuItemBuilder | null](#apendMenuItem)
- [appendMenuItemAtTheEnd MenuItemBuilder | null](#appendMenuItemAtTheEnd)
- [addMenuItemToModel() void](#addMenuItemToModel)
- [removeMenuElementById boolean](#removeMenuElementById)
- [removeMenuElementByPath MenuBuilder](#removeMenuElementByPath)
- [addMenuSeparator() void](#addMenuSeparator)
- [onClick() MenuItemBuilder](#onClick)
- [`addShortcut() MenuItemBuilder`](#addshortcut)
- [setApplicationMenu() void](#setapplicationmenu)
- [setApplicationMenuWithJson() void](#setapplicationmenuwithjson)

- ### getMenus
	- Returns:
		- Array\<EquoMenu>
	- Description:
		- Get all Equo menus from the under construction Menu.
	- [Use example](equo-application-menu-examples.md#Using-the-getMenus-method.)
	<br>

- ### withMainMenu
	- Params:
		- label :string
	- Returns:
		- MenuBuilder
	- Description: 
		-Create a root menu.
	- [Use example](equo-application-menu-examples.md#Using-the-withMainMenu-method.)
	<br>

- ### addMenu
- Params:
		- label :string
	- Returns:
		- MenuBuilder
	- Description:
		- Add a new menu that will contain other sub menus.
	- [Use example](equo-application-menu-examples.md#Using-the-addMenu-method.)
	<br>

- ### apendMenu
	- Params:
		- menuPath :string
		- indexToAddItem :number
		- menuName :string
	- Returns:
		- MenuBuilder
		- null
	- Description:
		- Append menu using path location. If menuPath does not exists return null.
	- [Use example](Link-to-use-example)
	<br>

- ### addMenuItem
	- Params:
		- label :string
	- Returns:
		- MenuItemBuilder
		- null
	- Description:
		- Add a new menu that will contain other menus. (PROBAR POR QUE DEVUELVE NULL)
	- [Use example](equo-application-menu-examples.md#Using-the-addMenuItem-method.)
	<br>

- ### apendMenuItem
	- Params:
		- menuPath :string
		- indexToAddItem :number
		- menuItemName :string
	- Returns:
		- MenuItemBuilder
		- null
	- Description:
		- Append menu item using path location. If menuPath does not exists return null.
	- [Use example](Link-to-use-example)
	<br>

- ### appendMenuItemAtTheEnd
	- Params:
		- menuPath :string
		- menuItemName :string
	- Returns:
		- MenuItemBuilder
		- null
	- Description:
		- Append menu item at the end. If menuPath does not exists return null. (PROBAR CUANDO PUEDE RETORNAL NULL)
	- [Use example](Link-to-use-example)
	<br>

- ### addMenuItemToModel
	- Params:
		- parentMenuId :string
		- index :number
		- menuItem :EquoMenu
	- Returns:
		- void
	- Description:
		- Add a new EquoMenu to the model using the parent menu id. If the parent menu id exists in the constructor, it will be added at the specified parent index.
	- [Use example](Link-to-use-example)
	<br>

- ### removeMenuElementById
	- Params:
		- menuToRemoveId :string
	- Returns:
		- boolean
	- Description:
		- Remove menu element by id. If exists menu id it will remove and return true.
 
	- [Use example](Link-to-use-example)
	<br>

- ### removeMenuElementByPath
	- Params:
		- menuNamePathToRemove :string
	- Returns:
		- MenuBuilder
	- Description:
		- Remove menu element by path. If exists menu id it will remove and return true. (QUE PASA CUANDO SE INTENTA REMOVER UN PATH QUE NO EXISTE?)
	- [Use example](Link-to-use-example)
	<br>

- ### addMenuSeparator
	- Returns:
		- MenuItemSeparatorBuilder
	- Description:
		- Add separator between menus.
	- [Use example](Link-to-use-example)
	<br>

- ### onClick
	- Params:
		- action :string | () => void :Function
	- Returns:
		- MenuItemBuilder
	- Description:
		- Add action in menu element. (QUE ACCIONES HAY DEFINIDAS?)
	- [Use example](Link-to-use-example)
	<br>

- ### addShortcut
	- Params:
		- shortcut :string
	- Returns:
		- MenuItemBuilder
	- Description:
		- Add shortcut in menu element.
	- [Use example](Link-to-use-example)
	<br>

- ### setApplicationMenu
	- Params:
		- ?(ws: EquoWebSocket, json: JSON) {} :Function
	- Returns:
		- void
	- Description:
		- Set the application menu to the current model. Optionally, pass a function parameter for custom actions after creating the menu through an EquoWebSocket.
	- [Use example](Link-to-use-example)
	<br>

- ### setApplicationMenuWithJson
	- Params:
		- json :JSON
	- Returns:
		- void
	- Description:
		- Set the application menu from a json format to the current model. (QUE PASA CUANDO HAY UN MENU EN CONSTRUCCION Y USO ESTO?)
	- [Use example](Link-to-use-example)
	<br>

#### MenuItemBuilder
- [withMainMenu() MenuBuilder](#withMainMenu-1)
- [addMenu() MenuBuilder](#addMenu-1)
- [addMenuItem() MenuItemBuilder](#addMenuItem-1)
- [addMenuSeparator() void](#addMenuSeparator-1)
- [onClick() MenuItemBuilder](#onClick-1)
- [addShortcut() MenuItemBuilder](#addShortcut-1)
- [setApplicationMenu() void](#setApplicationMenu-1)

- ### withMainMenu
	- Params:
		- label :string
	- Returns:
		- MenuBuilder
	- Description: 
		-Create a root menu.
	- [Use example](equo-application-menu-examples.md#Using-the-withMainMenu-method.)
	<br>

- ### addMenu
- Params:
		- label :string
	- Returns:
		- MenuBuilder
	- Description:
		- Add a new menu that will contain other sub menus.
	- [Use example](equo-application-menu-examples.md#Using-the-addMenu-method.)
	<br>

- ### addMenuItem
	- Params:
		- label :string
	- Returns:
		- MenuItemBuilder
		- null
	- Description:
		- Add a new menu that will contain other menus. (PROBAR POR QUE DEVUELVE NULL)
	- [Use example](equo-application-menu-examples.md#Using-the-addMenuItem-method.)
	<br>

- ### addMenuSeparator
	- Returns:
		- MenuItemSeparatorBuilder
	- Description:
		- Add separator between menus.
	- [Use example](Link-to-use-example)
	<br>

- ### onClick
	- Params:
		- action :string | () => void :Function
	- Returns:
		- MenuItemBuilder
	- Description:
		- Add action in menu element. (QUE ACCIONES HAY DEFINIDAS?)
	- [Use example](Link-to-use-example)
	<br>

- ### addShortcut
	- Params:
		- shortcut :string
	- Returns:
		- MenuItemBuilder
	- Description:
		- Add shortcut in menu element.
	- [Use example](Link-to-use-example)
	<br>

- ### setApplicationMenu
	- Params:
		- ?(ws: EquoWebSocket, json: JSON) {} :Function
	- Returns:
		- void
	- Description:
		- Set the application menu to the current model. Optionally, pass a function parameter for custom actions after creating the menu through an EquoWebSocket.
	- [Use example](Link-to-use-example)
	<br>

#### MenuItemSeparatorBuilder
- [addMenu() MenuBuilder](#addMenu-2)
- [addMenuItem() MenuItemBuilder](#addMenuItem-2)

- ### addMenu
- Params:
		- label :string
	- Returns:
		- MenuBuilder
	- Description:
		- Add a new menu that will contain other sub menus.
	- [Use example](equo-application-menu-examples.md#Using-the-addMenu-method.)
	<br>

- ### addMenuItem
	- Params:
		- label :string
	- Returns:
		- MenuItemBuilder
		- null
	- Description:
		- Add a new menu that will contain other menus. (PROBAR POR QUE DEVUELVE NULL)
	- [Use example](equo-application-menu-examples.md#Using-the-addMenuItem-method.)
	<br>

#### EquoMenu
- [fillFromJSON() void](#fillFromJSON)
- [addChildren() number](#addChildren)
- [addChildrenAtIndex() boolean](#addChildrenAtIndex)
- [setRunnable() void](#setRunnable)
- [setType() void](#setType)
- [setTitle() void](#setTitle)
- [setAction() void](#setAction)
- [setShortcut() void](#setShortcut)
- [setChildren() void](#setChildren)
- [getId() string](#getId)
- [getType() string](#getType)
- [getTitle() string](#getTitle)
- [getTitleMenus() Array\<string>](#getTitleMenus)
- [getAction() string](#getAction)
- [getShortcut() string](#getShortcut)
- [getChildren() Array\<EquoMenu>](#getChildren)
- [removeMenuItemOfIndex() void](#removeMenuItemOfIndex)
- [removeMenuItemById() void](#removeMenuItemById)

- ### fillFromJSON
	- Params:
		- json :string
	- Returns:
		- void
	- Description:
		- Initialice EquoMenu from json.
	- [Use example](Link-to-use-example)
	<br>

- ### addChildren
	- Params:
		- children :EquoMenu
	- Returns:
		- number
	- Description:
		- Add children in EquoMenu at the end. It will not be added if there is another child with the same title. Returns -1 if it is not added or the index to which it was added.
	- [Use example](Link-to-use-example)
	<br>

- ### addChildrenAtIndex
	- Params:
		- index :number
		- children :EquoMenu
	- Returns:
		- boolean
	- Description:
		- Add children in EquoMenu at the index. It will not be added if there is another child with the same title.
	- [Use example](Link-to-use-example)
	<br>

- ### setRunnable
	- Params:
		- runnable :() => void
	- Returns:
		- void
	- Description:
		- Set specific runnable for menu action.
	- [Use example](Link-to-use-example)
	<br>

- ### setType
	- Params:
		- type :string
	- Returns:
		- void
	- Description:
		- Set the menu type. Valid types are EquoMenu or EquoMenuItem.
	- [Use example](Link-to-use-example)
	<br>

- ### setTitle
	- Params:
		- title :string
	- Returns:
		- void
	- Description:
		- Set title for menu.
	- [Use example](Link-to-use-example)
	<br>

- ### setAction
	- Params:
		- action :string
	- Returns:
		- void
	- Description:
		- Set action id for menu.
	- [Use example](Link-to-use-example)
	<br>

- ### setShortcut
	- Params:
		- shortcut :string
	- Returns:
		- void
	- Description:
		- Set shortcut for menu.
	- [Use example](Link-to-use-example)
	<br>

- ### setChildren
	- Params:
		- childrens :\<EquoMenu>
	- Returns:
		- void
	- Description:
		- Set childrens for menu.
	- [Use example](Link-to-use-example)
	<br>

- ### getId
	- Returns:
		- string
	- Description:
		- Get menu id.
	- [Use example](Link-to-use-example)
	<br>

- ### getType
	- Returns:
		- string
	- Description:
		- Get menu type. Returns EquoMenu or EquoMenuItem.
	- [Use example](Link-to-use-example)
	<br>

- ### getTitle
	- Returns:
		- string
	- Description:
		- Get menu title.
	- [Use example](Link-to-use-example)
	<br>

- ### getTitleMenus
	- Returns:
		- Array\<string>
	- Description:
		- Get all menu titles in the EquoMenu from all childrends.
	- [Use example](Link-to-use-example)
	<br>

- ### getAction
	- Returns:
		- string
	- Description:
		- Get action id from menu.
	- [Use example](Link-to-use-example)
	<br>

- ### getShortcut
	- Returns:
		- string
	- Description:
		- Get shortcut from menu.
	- [Use example](Link-to-use-example)
	<br>

- ### getChildren
	- Returns:
		- Array\<EquoMenu>
	- Description:
		- Get childrens from menu.
	- [Use example](Link-to-use-example)
	<br>

- ### removeMenuItemOfIndex
	- Params:
		- index :number
	- Returns:
		- void
	- Description:
		- Remove children at the index.
	- [Use example](Link-to-use-example)
	<br>

- ### removeMenuItemById
	- Params:
		- menuItemId :string
	- Returns:
		- void
	- Description:
		- Remove children by id.
	- [Use example](Link-to-use-example)
	<br>
